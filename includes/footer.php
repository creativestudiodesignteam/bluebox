<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <p class="copy">
                    <a class="redesocial" href=""><i class="fab fa-facebook-f"></i></a>
                    <a class="redesocial" href=""><i class="fab fa-linkedin-in"></i></a>
                </p>
            </div>
            <div class="col-lg-6">
                <p class="dev">Desenvolvido por <a href="http://creativestudiodesigner.com.br" target="_BLANK">Creative Studio Design</a></p>
            </div>
        </div>
    </div>
</footer>

 <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>        
            <!-- 16:9 aspect ratio -->
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always" allow="autoplay"></iframe>
            </div>
      </div>
    </div>
  </div>
</div> 