<title>Bluebox</title>
<meta charset="UTF-8">
<meta name="author" content="">
<meta name="description" content="">
<meta name="robots" content="noarchive">
<meta name="reply-to" content="c">
<meta name="keywords" content="">
<meta property="og:locale" content="pt-br">
<meta property="og:url" content="">
<meta property="og:title" content="">
<meta property="og:site_name" content="">
<meta property="og:description" content="">
<meta property="og:image" content="">
<meta property="og:image:type" content="image/png">
<meta property="og:image:width" content="1600">
<meta property="og:image:height" content="1200">
<meta property="og:type" content="website">
<meta http-equiv="Content-Language" content="pt-br">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet"> 
<link rel="stylesheet" href="assets/vendor/owl-carousel/assets/owl.carousel.min.css">
<link rel="stylesheet" href="assets/vendor/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/animate.css">
<link rel="stylesheet" href="assets/fontes/font/flaticon.css">
<link rel="stylesheet" href="assets/css/menu_sideslide.css">
<link rel="shortcut icon" href="assets/images/favicon.png" />
<link rel="stylesheet" href="assets/css/animate.css">
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/timeline.css">