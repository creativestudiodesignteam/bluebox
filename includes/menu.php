        <div>
            <div class="menu-wrap">
                <nav class="menu" style="padding-top: 50px;">
                    <div class="icon-list">
                        <a href="#header">
                            <img src="assets/images/logo-original.png" style="max-width: 175px" alt="Bluebox" title="Bluebox" />
                        </a>
                        <a href="#header-home" class="removeBody" title="Home"><span>Home</span></a>
                        <a href="#como-funciona" class="removeBody" title="O que oferecemos"><span>O que oferecemos</span></a>
                        <a href="#objetivos" class="removeBody" title="Quem somos"><span>Quem somos</span></a>
                        <a href="#promotions" class="removeBody" title="Vantagens"><span>Vantagens</span></a>
                        <a href="#contact" class="removeBody" title="Entrar em contato"><span>Entrar em contato</span></a>
                    </div>
                </nav>
                <button class="close-button" id="close-button">Close Menu</button>
            </div>
            <button class="menu-button " id="open-button"></button>
        </div>

        <nav class="navbar navbar-expand-lg navbar-light nav-color">
            <div class="container">
                <a class="navbar-brand" href="#"><img src="assets/images/logo-original.png" style="max-width: 175px" alt="Bluebox" title="Bluebox"></a>
                

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#header-home" title="Home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#como-funciona" title="O que oferecemos">O que oferecemos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#objetivos" title="Quem somos">Quem somos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#vantagens" title="Vantagens">Vantagens</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#contact" title="Entrar em contato">Entrar em contato</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>