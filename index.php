<!DOCTYPE html>
<html lang="en">
    <?php include('includes/head.php') ?>
<head>
</head>

<body>
    <header id="header-home">
        
        <?php include('includes/menu.php') ?>
        
        <div class="img">
            <img class="img-fluid" src="assets/images/slide.png" alt="">
        </div>
        <div class="top-header">
            <div class="header-top">
                <div class="itemslide">
                    <div class="container">
                        <div class="col-lg-8">
                            <div class="description">
                                <h3 class="title">um jeito</h3>
                                <h3 class="title space-title1">diferente</h3>
                                <h3 class="title space-title2">de fazer</h3>
                                <h3 class="title space-title3 position-relative">
                                    <div class="bar align-bar">negócio</div>
                                </h3>
                                
                                <p class="text-white text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum, iure eum corporis commodi placeat sit officia dicta illo quae nulla? dicta illo quae nulla</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section id="newsletter">
        <div class="container padding-align">
            <div class="row">
                <div class="col-lg-6 align-self-center">
                    <h3 class="textnews">
                        quer receber noticias<br> e novidades? cadastre-se<br> em nossas <span class="bar align-bar">newsletter</span>
                    </h3>
                    <p>Lorem ipsum dolor sit amet, consectetur<br>adipiscing elit, sed do eiusmod tempor</p>
                </div>
                <div class="col-lg-6 align-self-center">
                    <form action="">
                        <div class="row">
                            <div class="col-lg-6 p-0">
                                <input type="email" name="" placeholder="Digite seu E-mail" style="font-size: 14px" class="form-control format-inputbtn" id="">
                            </div>
                            <div class="col-lg-6 p-0  text-center">
                                <button class="btn-default format-inputbtn text-uppercase m-0" style="font-size: 14px"><i class="fas fa-paper-plane"></i> cadastrar agora</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section id="como-funciona">
        <div class="container-fluid">
            <div class="row">
                <div class="offset-lg-4 col-lg-8">
                    <h2 class="title">Como <span class="bar align-bar">funciona</span></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 p-0">
                    <div class="timeline d-none d-md-block">
                        <div class="bg-line"></div>
                        <a href="#." onclick="showbox('#box-one')" class="ball -one"></a>
                        <a href="#." onclick="showbox('#box-two')" class="ball -two"></a>
                        <a href="#." onclick="showbox('#box-three')" class="ball -three"></a>

                        <div class="row">
                            <div id="box-one" class="col-lg-2 box-position box-position-one">
                                <div class="box">
                                    <i class="flaticon-man"></i>
                                <p>
                                    <span class="font-weight-bold">Acessoria</span><br>
                                    A BlueBox disponibiliza assessoria e a plataforma contendo uma variedade de produtos do segmento da empresa cadastrada.
                                </p>
                                </div>
                            </div>
                            <div id="box-two" class="col-lg-2 d-none box-position box-position-two">
                                <div class="box">
                                    <i class="flaticon-man"></i>
                                <p>
                                    <span class="font-weight-bold">Acessoria</span><br>
                                    A BlueBox disponibiliza assessoria e a plataforma contendo uma variedade de produtos do segmento da empresa cadastrada.
                                </p>
                                </div>
                            </div>
                            <div id="box-three" class="col-lg-2 d-none box-position box-position-three">
                                <div class="box">
                                    <i class="flaticon-man"></i>
                                <p>
                                    <span class="font-weight-bold">Acessoria</span><br>
                                    A BlueBox disponibiliza assessoria e a plataforma contendo uma variedade de produtos do segmento da empresa cadastrada.
                                </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
        <div class="container mostra-mobile mt-5">
            <div class="row timeline">
                <div class="col-lg-12 box-position">
                    <div class="box">
                        <i class="flaticon-man"></i>
                    <p>
                        <span class="font-weight-bold">Acessoria</span><br>
                        A BlueBox disponibiliza assessoria e a plataforma contendo uma variedade de produtos do segmento da empresa cadastrada.
                    </p>
                    </div>
                </div>
                <div class="col-lg-12 box-position">
                    <div class="box">
                        <i class="flaticon-man"></i>
                    <p>
                        <span class="font-weight-bold">Acessoria</span><br>
                        A BlueBox disponibiliza assessoria e a plataforma contendo uma variedade de produtos do segmento da empresa cadastrada.
                    </p>
                    </div>
                </div>
                <div class="col-lg-12 box-position">
                    <div class="box">
                        <i class="flaticon-man"></i>
                    <p>
                        <span class="font-weight-bold">Acessoria</span><br>
                        A BlueBox disponibiliza assessoria e a plataforma contendo uma variedade de produtos do segmento da empresa cadastrada.
                    </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="phrasestruct">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="text ls-3">
                        <span class="fine">
                            podemos até ser
                        </span><br>
                        uma caixa mas<br>
                        <span class="bar align-bar">pensamos fora dela</span>
                    </h1>
                    <a href="#." class="btn-transparent text-uppercase font-weight-bold">conheça as vantagens <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </section>

    <section id="objetivos">
        <div class="size-defaut what">
            <h1 class="title">o que é <br> a <span class="bar align-bar">bluebox</span></h1>
            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laudantium debitis fuga eligendi, nemo asperiores repellat non et ad expedita possimus a aspernatur molestias totam similique. Itaque odio sed fugit quae.</p>
        </div>
        <div class="size-defaut objective text-white">
            <h1 class="title">nosso<br><span class="bar align-bar">objetivo</span></h1>
            <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Laudantium debitis fuga eligendi, nemo asperiores repellat non et ad expedita possimus a aspernatur molestias totam similique. Itaque odio sed fugit quae.</p>
        </span>
    </section>

    <section id="vantagens">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-left">
                    <h2 class="title">
                        conheça todas as <br> vantagens na <span class="bar align-bar">bluebox</span>
                    </h2>
                </div>
                <div class="col-lg-6 text-right">
                    <a href="#contact" class="btn-default text-uppercase font-weight-bold">entrar em contato</a>
                </div>
            </div>
            <div class="row mt-5 d-none d-md-block">
                <div class="col-md-12">
                    <div class="main-timeline12">
                        <div class="col-md-2 col-sm-4 timeline">
                            <span class="timeline-icon">
                                <i class="flaticon-consultation"></i>
                            </span>
                            <div class="border"></div>
                            <div class="timeline-content">
                                <p class="description">Assessoria especializada no segmento de cada cliente.</p>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4 timeline">
                            <div class="timeline-content">
                                <p class="description">Pesquisa e comparação de produtos.</p>
                            </div>
                            <div class="border"></div>
                                <span class="timeline-icon">
                                    <i class="flaticon-graph"></i>
                                </span>
                        </div>
                        <div class="col-md-2 col-sm-4 timeline">
                            <span class="timeline-icon">
                                <i class="flaticon-tracking"></i>
                            </span>
                            <div class="border"></div>
                            <div class="timeline-content">
                                <p class="description">Necessidade de apenas 1 pedido para adquirir de vários fornecedores.</p>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4 timeline">
                            <div class="timeline-content">
                                <p class="description">Dashboard com informações detalhadas das compras.</p>
                            </div>
                            <div class="border"></div>
                            <span class="timeline-icon">
                                <i class="flaticon-layout"></i>
                            </span>
                        </div>
                        <div class="col-md-2 col-sm-4 timeline">
                            <span class="timeline-icon">
                                <i class="flaticon-life-insurance"></i>
                            </span>
                            <div class="border"></div>
                            <div class="timeline-content">
                                <p class="description">Segurança na compra dos produtos</p>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4 timeline">
                            <div class="timeline-content">
                                <p class="description">Redução de custos na aquisição de produtos (Clube de compras).</p>
                            </div>
                            <div class="border"></div>
                            <span class="timeline-icon">
                                <i class="flaticon-piggy-bank"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-5 mostra-mobile">
                <div class="col-lg-12 text-center pl-5 pr-5">
                    <i class="flaticon-consultation fs-80"></i>
                    <p>
                        Assessoria especializada no segmento de cada cliente.
                    </p>
                </div>
                <div class="col-lg-12 mt-3 text-center pl-5 pr-5">
                    <i class="flaticon-graph fs-80"></i>
                    <p>
                        Pesquisa e comparação de produtos.
                    </p>
                </div>
                <div class="col-lg-12 mt-3 text-center pl-5 pr-5">
                    <i class="flaticon-tracking fs-80"></i>
                    <p>
                        Necessidade de apenas 1 pedido para adquirir de vários fornecedores.
                    </p>
                </div>
                <div class="col-lg-12 mt-3 text-center pl-5 pr-5">
                    <i class="flaticon-layout fs-80"></i>
                    <p>
                        Dashboard com informações detalhadas das compras.
                    </p>
                </div>
                <div class="col-lg-12 mt-3 text-center pl-5 pr-5">
                    <i class="flaticon-life-insurance fs-80"></i>
                    <p>
                        Segurança na compra dos produtos
                    </p>
                </div>
                <div class="col-lg-12 mt-3 text-center pl-5 pr-5">
                    <i class="flaticon-piggy-bank fs-80"></i>
                    <p>
                        Redução de custos na aquisição de produtos (Clube de compras)
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="title">
                        entre em contato<br>em breve <span class="bar align-bar">retornaremos!</span>
                    </h2>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-lg-12 mb-5  wow fadeInUp">
                    <form action="" id="sendmessage" method="post">
                        <div class="row">
                            <div class="col-lg-12">
                                <label for="">Nome completo</label>
                                <input name="name" class="form-control" placeholder="Digite seu nome" type="text" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <label for="">E-mail</label>
                                <input name="email" class="form-control" placeholder="Digite seu e-mail" type="email" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <label for="">Telefone</label>
                                <input name="phone" class="form-control phone-form" placeholder="Digite seu telefone" type="text" required>
                            </div>
                            <div class="col-lg-6">
                                <label for="">Assunto</label>
                                <input name="subject" class="form-control" placeholder="Digite o Assunto" type="text" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <label for="">Mensagem</label>
                                <textarea name="message" class="form-control message" placeholder="Mensagem" id="message" cols="30" rows="10" required></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn-default w-100 float-right h-100 text-uppercase">Enviar mensagem</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>                    
    </section>                      
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>

    <script>
        function showbox(id){
            $('.box-position').removeClass('d-none');
            $('.box-position').fadeOut();
            $(id).fadeIn();
        }
    </script>
    
</body>

</html>