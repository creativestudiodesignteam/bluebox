function hoverimg(id, style){
  $(id).attr('style', style)
}

function register(form) {
  var postData = $(form).serializeArray();
  var formURL = $(form).attr("action");
  var validate = 0;
  $('#status_response').removeClass('d-none');
  $('form' + form + ' .form-control[required]').each(function () {
    if (!$(this).val()) {
      alertmsg('O campo ' + $(this).attr('title') + ' é obrigatório!', 'alert-danger');
      $(this).addClass('is-invalid');
      $('#status_response').addClass('d-none');
      validate++;
      return false;
    }
  });

  if (validate == 0) {
    $.ajax({
      type: "POST",
      url: formURL,
      data: postData,
      dataType: 'json',
      success: function (data) {
        var response = data['response'];
        alertmsg(response['mensagem'], response['classe'])

        if (response['result'] == 'error') {
          $('.validate-input').each(function () {
            if ($(this).val() == '') {
              $(this).addClass('is-invalid');
            }
          });
          return false;
        } else {
          window.location.replace(response['redirect']);
        }
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        alertmsg('Ocorreu um erro inesperado, por favor tente mais tarde!', 'alert-danger');
        return false;
      }
    });
  }

}

function alertmsg(msg, classe) {
  $('#status_response').addClass('d-none');
  $('#response').html('<div class="alert-dismissible fade show p-2" role="alert">' + msg + '</div>');
  $('#response').removeClass('alert alert-danger');
  $('#response').removeClass('alert alert-success');
  $('#response').addClass('alert ' + classe);
  $("#response").slideDown();
  setTimeout(function () {
    $("#response").slideUp();
  }, 4000);
}


  $(document).ready(function () {
    $('.owl-depoimentos').owlCarousel({
        loop:true,
        margin:10,
        nav: true,
        dots: false,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    })
    $('.owl-about').owlCarousel({
      loop:true,
      margin:10,
      nav: true,
      dots: false,
      navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
      responsive:{
          0:{
              items:1
          },
          600:{
              items:2
          },
          1000:{
              items:3
        }
      }
  })
  
  $('.owl-unistimg').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    dots: false,
    navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 1
      }
    }
  })

  $('.owl-units').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 1
      },
      1000: {
        items: 2
      }
    }
  });

    $('.owl-team2').owlCarousel({
      loop: true,
      margin: 10,
      nav: true,
      navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 3
        },
        1000: {
          items: 5
        }
      }
    })

  $('.owl-team').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3
      },
      1000: {
        items: 4
      }
    }
  })

  $(".owl-carousel").owlCarousel({
    loop: true,
    items: 1,
    margin: 0,
    stagePadding: 0,
    autoplay: false,
    autoplayTimeout: 7000,
    autoplayHoverPause: true,
    animateIn: 'fadeInUp',
    /* animateOut: 'fadeInDown', */
    transitionStyle: 'fadeIn',
    nav: false,
    navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"]

  });

  dotcount = 1;
  jQuery('.owl-dot').each(function () {
    if (dotcount < 5) {
      jQuery(this).addClass('dotnumber' + dotcount);
      jQuery(this).attr('data-info', dotcount);
      dotcount = dotcount + 1;
    }
  });

  slidecount = 1;

  jQuery('.owl-item').not('.cloned').each(function () {
    jQuery(this).addClass('slidenumber' + slidecount);
    slidecount = slidecount + 1;
  });

  jQuery('.owl-dot').each(function () {
    grab = jQuery(this).data('info');
    slidegrab = jQuery('.slidenumber' + grab + ' .description').attr('alt');
    jQuery(this).css("background-image", "url(" + slidegrab + ")");
  });

  amount = $('.owl-dot').length;
  gotowidth = 100 / amount;
  //jQuery('.owl-dot').css("height", gotowidth + "%");
  jQuery('.owl-dot').css("height", "94px");
  //jQuery('.owl-dot').css("margin-bottom:", "10px");

});

$('#clients-slide').owlCarousel({
  loop: true,
  margin: 10,
  responsiveClass: true,
  autoplay: true,
  autoplayTimeout: 4000,
  autoplayHoverPause: true,
  center: true,
  dots: true,
  responsive: {
    0: {
      items: 2,
      nav: false
    },
    600: {
      items: 3,
      nav: false
    },
    1000: {
      items: 5,
      nav: true,
      navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"]
    }
  }
});


$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});

$(window).scroll(function () {
  var scroll = $(window).scrollTop();
  if (scroll >= 50) {
    $(".navbar").addClass("fixed-top");
    $(".navbar").addClass("bg-scroll");
    $("#logotroca").attr("src", 'assets/images/logo-original.png');
  } else {
    $(".navbar").removeClass("fixed-top");
    $(".navbar").removeClass("bg-scroll");
    $("#logotroca").attr("src", 'assets/images/logo.png');
  }
});

var $doc = $('html, body');
$('.nav-link').click(function () {
  $doc.animate({
    scrollTop: $($.attr(this, 'href')).offset().top
  }, 500);
  return false;
});

